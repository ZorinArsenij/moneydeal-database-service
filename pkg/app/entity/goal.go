package entity

import (
	"time"
)

// Goal store information about goal
type Goal struct {
	ID      string
	Title   string
	Balance int64
	Amount  int64
	Finish  time.Time
	Photo   string
}

// GoalPayment store payment associated with goal
type GoalPayment struct {
	GoalID     string
	AccountID  string
	Title      string
	Amount     int64
	Date       time.Time
	GoalFinish time.Time
}

package adapter

import (
	"strings"
	"time"

	"gitlab.com/ZorinArsenij/moneydeal-database-service/pkg/app/entity"

	"github.com/gocql/gocql"
	"gitlab.com/ZorinArsenij/moneydeal-database-service/pkg/app/infrastructure/repository"
	"gitlab.com/ZorinArsenij/moneydeal-database-service/pkg/errors"
)

type Category struct {
	repo Repository
}

func NewCategory(repo Repository) Category {
	return Category{
		repo: repo,
	}
}

// Create add new category to account
func (c Category) Create(accountID string, category *entity.Category) error {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	categoryUUID := gocql.TimeUUID()
	category.ID = categoryUUID.String()

	return c.repo.Insert(repository.QueryCategoryInsert, categoryUUID, accountUUID, strings.ToLower(category.Name),
		0, time.Now(), category.IsExpense, category.ImagePath)
}

// List get list of account categories
func (c Category) List(accountID string, isExpense bool, name string, limit uint64) (*[]entity.Category, error) {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := c.repo.Select(repository.QueryCategoryByAccountID, accountUUID, isExpense, name, limit)

	categories := make([]entity.Category, 0, len(list))
	for _, l := range list {
		categories = append(categories, entity.Category{
			ID:         l["id"].(gocql.UUID).String(),
			Name:       l["name"].(string),
			Amount:     l["amount"].(int64),
			IsExpense:  l["is_expense"].(bool),
			LastUpdate: l["last_update"].(time.Time),
			ImagePath:  l["image_path"].(string),
		})
	}

	return &categories, nil
}

func (c Category) CreatePayment(payment *entity.CategoryPayment) error {
	accountUUID, err := gocql.ParseUUID(payment.AccountID)
	if err != nil {
		return errors.ErrInvalidUUID
	}

	category := c.repo.Select(repository.QueryCategory, accountUUID, payment.IsExpense, payment.Name)
	if len(category) != 1 {
		return errors.ErrNotFound
	}

	newAmount := category[0]["amount"].(int64) + payment.Amount
	categoryUUID := category[0]["id"].(gocql.UUID).String()

	if err := c.repo.Update(repository.QueryCategoryUpdate, newAmount, time.Now(), accountUUID, payment.IsExpense, payment.Name); err != nil {
		return err
	}

	if err := c.repo.Insert(repository.QueryCategoryPaymentInsert, categoryUUID, accountUUID, payment.Title, payment.Type, payment.Amount, payment.Date); err != nil {
		return err
	}

	return nil
}

func (c Category) ListPayments(accountID, categoryID string, from time.Time, limit uint64) (*[]entity.CategoryPayment, error) {
	accountUUID, err := gocql.ParseUUID(accountID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	categoryUUID, err := gocql.ParseUUID(categoryID)
	if err != nil {
		return nil, errors.ErrInvalidUUID
	}

	list := c.repo.Select(repository.QueryCategoryPayment, accountUUID, categoryUUID, from, limit)

	payments := make([]entity.CategoryPayment, 0, len(list))
	for _, l := range list {
		payments = append(payments, entity.CategoryPayment{
			Title:  l["title"].(string),
			Type:   l["type"].(string),
			Amount: l["amount"].(int64),
			Date:   l["date"].(time.Time),
		})
	}

	return &payments, nil
}

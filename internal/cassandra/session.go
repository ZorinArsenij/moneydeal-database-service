package cassandra

import (
	"github.com/gocql/gocql"
)

// NewSession create new session connection with cassandra
func NewSession(addr, keyspace string) (*gocql.Session, error) {
	cfg := gocql.NewCluster(addr)
	cfg.Keyspace = keyspace

	session, err := gocql.NewSession(*cfg)
	if err != nil {
		return nil, err
	}

	return session, nil
}

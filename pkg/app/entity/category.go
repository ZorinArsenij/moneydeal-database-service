package entity

import "time"

//go:generate easyjson category.go

// Category store information about account
type Category struct {
	ID         string
	AccountID  string
	Name       string
	Amount     int64
	IsExpense  bool
	LastUpdate time.Time
	ImagePath  string
}

// CategoryPayment store payment associated with category
type CategoryPayment struct {
	CategoryID string
	AccountID  string
	Name       string
	IsExpense  bool
	Title      string
	Type       string
	Amount     int64
	Date       time.Time
}

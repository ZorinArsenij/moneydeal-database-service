package config

type Config struct {
	DatabaseAddress string
	Keyspace        string
}

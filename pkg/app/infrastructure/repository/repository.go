package repository

import (
	"github.com/gocql/gocql"
	"gitlab.com/ZorinArsenij/moneydeal-database-service/internal/cassandra"
	"gitlab.com/ZorinArsenij/moneydeal-database-service/internal/config"
)

const (
	QueryAccountIDByPhone    = "SELECT * FROM accounts_phone_id WHERE phone = ?"
	QueryAccountByID         = "SELECT * FROM accounts WHERE id = ?"
	QueryAccountUpdate       = "INSERT INTO accounts(id, firstname, lastname, photo_path) VALUES(?, ?, ?, ?)"
	QueryAccountInsert       = "INSERT INTO accounts(id, phone) VALUES(?, ?)"
	QueryAccountUpdateStatus = "UPDATE accounts_phone_id SET status = ? WHERE phone = ?"
	QueryAccountInsertPhone  = "INSERT INTO accounts_phone_id(phone, id, status) VALUES(?, ?, ?)"

	QueryGoalInsert        = "INSERT INTO goal(id, account_id, title, amount, balance, finish, photo) VALUES(?, ?, ?, ?, ?, ?, ?)"
	QueryGoalByAccountID   = "SELECT * FROM goal WHERE account_id = ? AND finish > ? LIMIT ?"
	QueryGoalBalance       = "SELECT balance FROM goal WHERE account_id = ? AND finish = ? AND id = ?"
	QueryGoalBalanceUpdate = "UPDATE goal SET balance = ? WHERE account_id = ? AND finish = ? AND id = ? IF EXISTS"
	QueryGoalPaymentInsert = "INSERT INTO goal_payment(goal_id, account_id, title, amount, date) VALUES(?, ?, ?, ?, ?)"
	QueryGoalPayment       = "SELECT * FROM goal_payment WHERE account_id = ? AND goal_id = ? AND date < ? LIMIT ?"

	QueryCategoryInsert        = "INSERT INTO category(id, account_id, name, amount, last_update, is_expense, image_path) VALUES(?, ?, ?, ?, ?, ?, ?)"
	QueryCategoryByAccountID   = "SELECT * FROM category WHERE account_id = ? AND is_expense = ? AND name > ? LIMIT ?"
	QueryCategory              = "SELECT id, amount FROM category WHERE account_id = ? AND is_expense = ? AND name = ?"
	QueryCategoryUpdate        = "UPDATE category SET amount = ?, last_update = ? WHERE account_id = ? AND is_expense = ? AND name = ? IF EXISTS"
	QueryCategoryPaymentInsert = "INSERT INTO category_payment(category_id, account_id, title, type, amount, date) VALUES(?, ?, ?, ?, ?, ?)"
	QueryCategoryPayment       = "SELECT * FROM category_payment WHERE account_id = ? AND category_id = ? AND date < ? LIMIT ?"
)

// Repository store session for cassandra
type Repository struct {
	conn *gocql.Session
}

// New create instance of repository
func New(cfg config.Config) (Repository, error) {
	conn, err := cassandra.NewSession(cfg.DatabaseAddress, cfg.Keyspace)
	if err != nil {
		return Repository{}, err
	}

	return Repository{
		conn: conn,
	}, nil
}

func (r Repository) Get(request string, result []interface{}, params ...interface{}) error {
	return r.conn.Query(request, params...).Scan(result...)
}

func (r Repository) Insert(request string, params ...interface{}) error {
	return r.conn.Query(request, params...).Exec()
}

func (r Repository) Update(request string, params ...interface{}) error {
	return r.conn.Query(request, params...).Exec()
}

func (r Repository) Select(request string, params ...interface{}) []map[string]interface{} {
	iter := r.conn.Query(request, params...).Iter()
	row := map[string]interface{}{}
	result := make([]map[string]interface{}, 0, iter.NumRows())

	for iter.MapScan(row) {
		result = append(result, row)
		row = map[string]interface{}{}
	}

	return result
}

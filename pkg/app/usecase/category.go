package usecase

import (
	"time"

	"gitlab.com/ZorinArsenij/moneydeal-database-service/pkg/app/entity"
)

type CategoryAdapter interface {
	Create(accountID string, category *entity.Category) error
	List(accountID string, isExpense bool, name string, limit uint64) (*[]entity.Category, error)
	CreatePayment(payment *entity.CategoryPayment) error
	ListPayments(accountID, categoryID string, from time.Time, limit uint64) (*[]entity.CategoryPayment, error)
}

// Category perform category use cases
type Category struct {
	adapter CategoryAdapter
}

// NewCategory create instance of Category
func NewCategory(adapter CategoryAdapter) Category {
	return Category{
		adapter: adapter,
	}
}

// Create add new category to account
func (c Category) Create(accountID string, category *entity.Category) error {
	return c.adapter.Create(accountID, category)
}

// List get list of categories
func (c Category) List(accountID string, isExpense bool, name string, limit uint64) (*[]entity.Category, error) {
	return c.adapter.List(accountID, isExpense, name, limit)
}

func (c Category) CreatePayment(payment *entity.CategoryPayment) error {
	return c.adapter.CreatePayment(payment)
}

func (c Category) ListPayments(accountID, categoryID string, from time.Time, limit uint64) (*[]entity.CategoryPayment, error) {
	return c.adapter.ListPayments(accountID, categoryID, from, limit)
}

package entity

//go:generate easyjson account.go

// AccountInfo store information about account
type AccountInfo struct {
	FirstName string
	LastName  string
	Phone     string
	PhotoPath string
	Status    bool
}

// Account store account id and info
type Account struct {
	AccountInfo
	ID string
}

package key

type contextKey struct {
	name string
}

var (
	// ContextLogger is a key for context
	// nolint: gochecknoglobals
	ContextLogger = contextKey{
		name: "logger",
	}
)
